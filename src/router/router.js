import Vue from 'vue';
import Router from 'vue-router';
import Users from '@/components/Users';
import UsersId from '@/components/UsersId';
import UsersAdd from '@/components/UsersAdd';
import UsersIdEdit from '@/components/UsersIdEdit';
import Albums from '@/components/Albums';
import AlbumsId from "@/components/AlbumsId";
import AlbumsIdEdit from "@/components/AlbumsIdEdit";
import AlbumsAdd from "@/components/AlbumsAdd";
import PhotoId from "@/components/PhotoId";

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: "/",
  routes: [
    {
      path: '/users',
      name: 'users',
      component: Users
    },
    {
      path: '/users/add',
      name: 'usersAdd',
      component: UsersAdd
    },
    {
      path: '/users/:id',
      name: 'usersId',
      component: UsersId
    },
    {
      path: '/users/:id/edit',
      name: 'usersIdEdit',
      component: UsersIdEdit
    },
    {
      path: '/albums',
      name: 'albums',
      component: Albums
    },
    {
      path: '/albums/:id',
      name: 'albumsId',
      component: AlbumsId
    },
    {
      path: '/albums/:id/edit',
      name: 'albumsIdEdit',
      component: AlbumsIdEdit
    },
    {
      path: "/albums/add",
      name: "albumsAdd",
      component: AlbumsAdd
    },
    {
      path: "/albums/:albumId/photo/:photoId",
      name: "photoId",
      component: PhotoId
    },
    {
      path: '*',
      redirect: '/users',
    },
  ]
})
