import Vue from 'vue';
import App from './App.vue';
import router from './router/router';
import store from './store/store';
import vuetify from './plugins/vuetify';
import axios from "axios";
import VueAxios from "vue-axios";
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

Vue.use(VueAxios, axios);
Vue.config.productionTip = false;

new Vue({
  axios,
  router,
  store,
  vuetify,
  components: { App },
  template: '<App/>',
  render: h => h(App)
}).$mount('#app')
